/**
 * vim: set ts=4 :
 * =============================================================================
 * SourceMod Sample Extension
 * Copyright (C) 2004-2008 AlliedModders LLC.  All rights reserved.
 * =============================================================================
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * As a special exception, AlliedModders LLC gives you permission to link the
 * code of this program (as well as its derivative works) to "Half-Life 2," the
 * "Source Engine," the "SourcePawn JIT," and any Game MODs that run on software
 * by the Valve Corporation.  You must obey the GNU General Public License in
 * all respects for all other code used.  Additionally, AlliedModders LLC grants
 * this exception to all derivative works.  AlliedModders LLC defines further
 * exceptions, found in LICENSE.txt (as of this writing, version JULY-31-2007),
 * or <http://www.sourcemod.net/license.php>.
 *
 * Version: $Id$
 */

#include "extension.h"
#include <string.h>
#include <cstring>
#include "lsl_cpp.h"
#include <vector>

/**
 * @file extension.cpp
 * @brief Implement extension code here.
 */

LSLext g_LSLext;		/**< Global singleton for extension's main interface */

SMEXT_LINK(&g_LSLext);


const sp_nativeinfo_t MyNatives[] = 
{
	{"createLSLoutlet",	createLSLoutlet},
	{"push_intSample", push_intSample},
	{"push_floatSample", push_floatSample},
	{"push_stringSample", push_stringSample},
	{NULL,			NULL},
};

static std::vector<lsl::stream_outlet> outlets;


void LSLext::SDK_OnAllLoaded()
{
	sharesys->AddNatives(myself, MyNatives);
}

cell_t createLSLoutlet(IPluginContext *pContext, const cell_t *params)
{

	char *outletName, *outletType, *channelLabels, *streamID;
	cell_t *channelsAddr;

	pContext->LocalToString(params[1], &outletName);
	pContext->LocalToString(params[2], &outletType);
	pContext->LocalToString(params[3], &channelLabels);
	cell_t rate = params[4];
	pContext->LocalToString(params[5], &streamID);

	// build stream ID from outlet name and type
	strcat(streamID, outletName);
	strcat(streamID, outletType);

	int numChannels = 0;
	bool isMarkerOutlet = false;
	bool isIntOutlet = false;
	bool isFloatOutlet = false;

	if(strncmp("string",outletType,6)==0 || strncmp("String",outletType,6)==0){
		isMarkerOutlet = true;
		numChannels = 1;
	}else if(strncmp("int",outletType,3)==0 || strncmp("Int",outletType,3)==0){
		isIntOutlet = true;

	}else if(strncmp("float",outletType,5)==0 || strncmp("Float",outletType,5)==0){
		isFloatOutlet = true;

	}else{
		return 0;
	}


	// Get number of channels from the labels
	if((isIntOutlet || isFloatOutlet) && strlen(channelLabels)>0){
		numChannels=1;
		char * pch;
		pch=strchr(channelLabels,';');
	  while (pch!=NULL)
	  {
	    numChannels++;
	    pch=strchr(pch+1,';');
	  }
	}else if(isIntOutlet || isFloatOutlet){
		return -1;
	}
	
	if(isIntOutlet)// Regular data rate outlet
	{
		lsl::stream_info info(outletName,outletType,numChannels,rate,lsl::cf_int32,streamID);
		// add some description fields
		info.desc().append_child_value("Game", "CS:GO");
		lsl::xml_element chns = info.desc().append_child("channels");

		char * label = std::strtok(channelLabels,";");
		while(label!=NULL)
		{
			chns.append_child("channel")
			.append_child_value("label",label);

			label = std::strtok(NULL,";");
		}

		outlets.emplace_back(info);
	}else if(isFloatOutlet)// Regular data rate outlet
	{
		lsl::stream_info info(outletName,outletType,numChannels,rate,lsl::cf_float32,streamID);
		// add some description fields
		info.desc().append_child_value("Game", "CS:GO");
		lsl::xml_element chns = info.desc().append_child("channels");

		char * label = std::strtok(channelLabels,";");
		while(label!=NULL)
		{
			chns.append_child("channel")
			.append_child_value("label",label);

			label = std::strtok(NULL,";");
		}

		outlets.emplace_back(info);
	}else if(isMarkerOutlet) //marker outlet, create single channel outlet of string format
	{
		outlets.emplace_back(lsl::stream_info(outletName,outletType,1,lsl::IRREGULAR_RATE,lsl::cf_string,streamID));
	}else{
		return -2;
	}


	return numChannels;
}

cell_t push_intSample(IPluginContext *pContext, const cell_t *params)
{
	char *streamID;
	pContext->LocalToString(params[1], &streamID);


	cell_t *c_sample;

	pContext->LocalToPhysAddr(params[2], &c_sample);

	float timestamp = sp_ctof(params[3]);

	for(auto& outlet: outlets){
		
		if(std::strcmp(outlet.info().source_id().c_str(),streamID)==0)
		{
			// convert cell_t to a regular float array to push it in the outlet.
			int numChannels = outlet.info().channel_count();
			int sample[numChannels];
			for(int i=0; i<numChannels; i++)
			{
				sample[i] = c_sample[i];
			}

			if(timestamp<0)
			{
				outlet.push_sample(sample);
			}else
			{
				outlet.push_sample(sample, timestamp);
			}
			return 0;
		}
		
	}

	

	return 1;
}

cell_t push_floatSample(IPluginContext *pContext, const cell_t *params)
{
	char *streamID;
	pContext->LocalToString(params[1], &streamID);


	cell_t *c_sample;

	pContext->LocalToPhysAddr(params[2], &c_sample);

	float timestamp = sp_ctof(params[3]);

	for(auto& outlet: outlets){
		
		if(std::strcmp(outlet.info().source_id().c_str(),streamID)==0)
		{
			// convert cell_t to a regular float array to push it in the outlet.
			int numChannels = outlet.info().channel_count();
			float sample[numChannels];
			for(int i=0; i<numChannels; i++)
			{
				sample[i] = sp_ctof(c_sample[i]);
			}

			if(timestamp<0)
			{
				outlet.push_sample(sample);
			}else
			{
				outlet.push_sample(sample, timestamp);
			}
			return 0;
		}
		
	}

	

	return 1;
}


cell_t push_stringSample(IPluginContext *pContext, const cell_t *params)
{
	char *streamID, *marker;
	pContext->LocalToString(params[1], &streamID);

	pContext->LocalToString(params[2], &marker);

	float timestamp = sp_ctof(params[3]);

	for(auto& outlet: outlets){
		
		if(std::strcmp(outlet.info().source_id().c_str(),streamID)==0)
		{
			std::string mrk(marker);

			if(timestamp<0)
			{
				outlet.push_sample(&mrk);
			}else
			{
				outlet.push_sample(&mrk, timestamp);
			}
			return 0;
		}
		
	}

	

	return 1;
}