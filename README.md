# CSGO LSL extension

# Dev Setup
## cpp c++ libraries
Will need these below
```bash
sudo apt install gcc-multilib g++-multilib
```

## Sourcemod source and checkout correct version
This extension does not compile on the latest of sourcemod, it has been tested with `sourcemod-1.7.0`
1. `git clone https://github.com/alliedmodders/sourcemod.git`
2. `git checkout tags/sourcemod-1.7.0`

## Get hl2sdk and checkout csgo branch
1. `git clone https://github.com/alliedmodders/hl2sdk.git`
2. `git checkout csgo`

## `Makefile`
Make sure that the `Makefile` paths are properly set:
* `SMSDK` should point to sourcemod source
* `HL2SDK_CSGO` should point to the hl2sdk (on csgo branch)

## liblsl
Make sure liblsl **32bit** is installed in your system. Unfortunately there are no prebuilt libraries for 32bit systems, you'll need to build liblsl yourself. Good luck...

Add `-fexceptions` flag in Makefile:
```
CPP_GCC4_FLAGS = -fvisibility-inlines-hidden -fexceptions
```

Add `-lstdc++` and link liblsl to line 164 of Makefile:
```
LINK += -shared -lstdc++ -llsl32
```

### building 32 bit liblsl (linux)
Requires cmake:
```
sudo snap install cmake --classic
```

Then:
```bash
git clone https://github.com/sccn/liblsl.git
```
Add the following to the top of `CMakeLists.txt` to make 32bit:
```
set(CMAKE_C_COMPILER "gcc")
set(CMAKE_C_FLAGS -m32)
set(CMAKE_CXX_COMPILER "gcc")
set(CMAKE_CXX_FLAGS -m32)
```

Finally:
```bash
cd liblsl
mkdir build && cd build
cmake .. -G "Unix Makefiles"
cmake --build . --config Release --target install
```

There will be a bunch of errors after it has created the shared libraries. You should be able to safely ignore those. Any errors before it has finished generating the library are not good.

Anyway, copy `liblsl32.so` and `liblsl32.so.X.X.X` to `/usr/lib/`

# Compiling
## Linux
Run `make` in this repository's top directory.

## Windows
Have not tried...

# Installing
You will need to first install sourcemod and metamod on your CSGO server.

* Copy the generated `LSLext.ext.so` and `LSLext.autoload` to `csgo/addons/sourcemod/extensions`
* To compile your own sourcepawn plugin using this extension, copy `LSLext.inc` to `csgo/addons/sourcemod/scripting/include`