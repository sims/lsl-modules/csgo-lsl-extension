/**
 * Creates an LSL outlet. If numChannels>0, the channel_format will be lsl::cf_float32. numChannels==0 then the channel format will be lsl::cf_string.
 *
 * @param outletName Name of the outlet (ex. "CSGO_Player0_steamID")
 * @param outletType Type of outlet in this format "int/float/string <description>" e.g." float positioning data".
 * @param channelLabels Channel names (ex. "x_position;y_position"). Ignored if string type outlet (marker).
 * @param rate Rate of data in Hz.
 * @param streamID Reference to put stream ID once outlet is created.
 * @return Number of channels of outlet based on channelLabels.
*/
native int createLSLoutlet(char[] outletName, char[] outletType, char[] channelLabels, int rate, char[] streamID);

/**
 * Pushes a sample to an outlet. Do not use for markers!
 *
 * @param outlet Outlet stream ID to push sample to.
 * @param sample data to send.
 * @param timestamp Float of local clock value. Default=-1.
 * @return Success code.
*/
native int push_intSample(char[] streamID, int[] sample, float timestamp=-1.0);

/**
 * Pushes a sample to an outlet. Do not use for markers!
 *
 * @param outlet Outlet stream ID to push sample to.
 * @param sample data to send.
 * @param timestamp Float of local clock value. Default=-1.
 * @return Success code.
*/
native int push_floatSample(char[] streamID, float[] sample, float timestamp=-1.0);

/**
 * Pushes a marker to an outlet.
 *
 * @param outlet Outlet stream ID to push sample to.
 * @param marker string to send.
 * @param timestamp Float of local clock value. Default=-1.
 * @return Success code.
*/
native int push_stringSample(char[] streamID, char[] marker, float timestamp=-1.0);